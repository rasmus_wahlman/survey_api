﻿using System;

namespace Survey.Api.Domain
{
    public class Answer
    {
        public Guid QuestionId { get; set; }
        public int Grade { get; set; }
    }
}
