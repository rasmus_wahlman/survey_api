﻿using System;

namespace Survey.Api.Domain
{
    public class Statistic
    {
        public Guid QuestionId { get; set; }
        public decimal AverageGrade { get; set; }
    }
}
