﻿using System;

namespace Survey.Api.Domain
{
    public class Question
    {
        public Guid Id { get; set; }
        public string QuestionKey { get; set; }
        public string Text { get; set; }
    }
}
