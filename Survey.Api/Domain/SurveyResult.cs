﻿using System.Collections.Generic;

namespace Survey.Api.Domain
{
    public class SurveyResult
    {
        public int NumberOfSurveysTaken { get; set; }
        public List<Statistic> AverageGrades { get; set; }
        public List<Answer> Answered { get; set; }
        public List<Question> Questions { get; set; }
    }
}
