﻿using Microsoft.EntityFrameworkCore;
using Survey.Api.Dtos;

namespace Survey.Api.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<QuestionDto> Questions { get; set; }
        public DbSet<AnswerDto> Answers { get; set; }
    }
}
