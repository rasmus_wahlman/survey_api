﻿using Survey.Api.Domain;
using Survey.Api.Mapping;
using Survey.Api.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.Api.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly IQuestionsRepository _questionsRepository;
        private readonly IAnswersRepository _answersRepository;
        public SurveyService(IQuestionsRepository questionsRepository, IAnswersRepository answersRepository)
        {
            _questionsRepository = questionsRepository;
            _answersRepository = answersRepository;
        }

        public async Task<List<Question>> GetSurveyAsync()
        {
            var survey = await _questionsRepository.GetSurveyAsync();
            return survey.Select(q => q.MapQuestionDtoToQuestion()).ToList();
        }

        public async Task<SurveyResult> GetSurveyResultByIdAsync(Guid id)
        {
            var result = await _answersRepository.GetResultAsync();
            var answered = await _answersRepository.GetResultFromSurveyById(id);

            var firstElement = result.FirstOrDefault();

            var surveyResult = new SurveyResult
            {
                NumberOfSurveysTaken = firstElement != null ? firstElement.Count : 0,
                AverageGrades = result.Select(r => new Statistic 
                { 
                    AverageGrade = Convert.ToDecimal(r.Sum)/Convert.ToDecimal(r.Count),
                    QuestionId = r.QuestionId 
                }).ToList(),
                Answered = answered.Select(a => a.MapAnswerDtoToAnswer()).ToList(),
                Questions = answered.Select(q => q.Question.MapQuestionDtoToQuestion()).ToList()
            };

            return surveyResult;
        }

        public async Task<Guid> CreateSurveyAnswersAsync(List<Answer> answers)
        {
            var createdAnswer = await _answersRepository.CreateAnswersAsync(answers.Select(a => a.MapAnswerDomainToAnswerDto()).ToList());
            return createdAnswer;
        }
    }
}
