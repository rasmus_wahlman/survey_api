﻿using Survey.Api.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Survey.Api.Services
{
    public interface ISurveyService
    {
        Task<List<Question>> GetSurveyAsync();
        Task<SurveyResult> GetSurveyResultByIdAsync(Guid id);
        Task<Guid> CreateSurveyAnswersAsync(List<Answer> answers);
    }
}
