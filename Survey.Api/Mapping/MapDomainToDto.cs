﻿using Survey.Api.Domain;
using Survey.Api.Dtos;

namespace Survey.Api.Mapping
{
    public static class MapDomainToDto
    {
        public static AnswerDto MapAnswerDomainToAnswerDto(this Answer answer)
        {
            return new AnswerDto 
            {
                Grade = answer.Grade,
                QuestionId = answer.QuestionId
            };
        }
    }
}
