﻿using Survey.Api.Contracts.Responses;
using Survey.Api.Domain;
using System.Linq;

namespace Survey.Api.Mapping
{
    public static class MapDomainToContracts
    {
        public static QuestionResponse MapQuestionToQuestionResponse(this Question question)
        {
            return new QuestionResponse
            {
                Id = question.Id,
                QuestionKey = question.QuestionKey,
                Text = question.Text
            };
        }

        public static SurveyResultResponse MapSurveyResultToSurveyResultResponse(this SurveyResult surveyResult)
        {
            return new SurveyResultResponse
            {
                NumberOfSurveysTaken = surveyResult.NumberOfSurveysTaken,
                AverageGrades = surveyResult.AverageGrades.Select(r => new Contracts.Responses.Statistic { AverageGrade = r.AverageGrade, QuestionId = r.QuestionId }).ToList(),
                Answered = surveyResult.Answered.Select(a => new AnswerResponse { Grade = a.Grade, QuestionId = a.QuestionId }).ToList(),
                Questions = surveyResult.Questions.Select(q => new QuestionResponse { Id = q.Id, QuestionKey = q.QuestionKey, Text = q.Text }).ToList()
            };
        }
    }
}
