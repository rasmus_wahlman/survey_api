﻿using Survey.Api.Domain;

namespace Survey.Api.Mapping
{
    public static class MapContractToDomain
    {
        public static Answer MapAnswerContractToAnswer(this Contracts.Requests.Answer answer)
        {
            return new Answer
            {
                QuestionId = answer.QuestionId,
                Grade = answer.Grade
            };
        }
    }
}
