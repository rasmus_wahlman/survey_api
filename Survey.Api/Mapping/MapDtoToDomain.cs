﻿using Survey.Api.Domain;
using Survey.Api.Dtos;

namespace Survey.Api.Mapping
{
    public static class MapDtoToDomain
    {
        public static Question MapQuestionDtoToQuestion(this QuestionDto questionDto)
        {
            return new Question
            {
                Id = questionDto.Id,
                QuestionKey = questionDto.QuestionKey,
                Text = questionDto.Text
            };
        }

        public static Answer MapAnswerDtoToAnswer(this AnswerDto answerDto)
        {
            return new Answer
            {
                Grade = answerDto.Grade,
                QuestionId = answerDto.QuestionId
            };
        }
    }
}
