﻿using Microsoft.AspNetCore.Mvc;
using Survey.Api.Contracts.Requests;
using Survey.Api.Mapping;
using Survey.Api.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyService _surveyService;
        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSurvey()
        {
            var survey = await _surveyService.GetSurveyAsync();
            return Ok(survey.Select(q => q.MapQuestionToQuestionResponse()));
        }

        [HttpGet("{surveyId}")]
        public async Task<IActionResult> GetSurveyById([FromRoute] Guid surveyId)
        {
            var result = await _surveyService.GetSurveyResultByIdAsync(surveyId);
            return Ok(result.MapSurveyResultToSurveyResultResponse());
        }

        [HttpPost]
        public async Task<IActionResult> CreateSurveyAnswer([FromBody] CreateSurveyAnswerRequest request)
        {
            var answers = request.Answers.Select(a => a.MapAnswerContractToAnswer()).ToList();
            var result = await _surveyService.CreateSurveyAnswersAsync(answers);
            return CreatedAtAction("GetSurveyById", new { surveyId = result }, result);
        }
    }
}
