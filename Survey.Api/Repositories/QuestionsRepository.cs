﻿using Microsoft.EntityFrameworkCore;
using Survey.Api.Data;
using Survey.Api.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Survey.Api.Repositories
{
    public class QuestionsRepository : IQuestionsRepository
    {
        private readonly DataContext _dataContext;

        public QuestionsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<QuestionDto>> GetSurveyAsync()
        {
            return await _dataContext.Questions.ToListAsync();
        }
    }
}
