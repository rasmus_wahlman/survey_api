﻿using Survey.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Survey.Api.Repositories
{
    public interface IAnswersRepository
    {
        Task<List<ResultDto>> GetResultAsync();
        Task<List<AnswerDto>> GetResultFromSurveyById(Guid id);
        Task<Guid> CreateAnswersAsync(List<AnswerDto> answers);
    }
}
