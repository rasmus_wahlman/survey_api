﻿using Survey.Api.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Survey.Api.Repositories
{
    public interface IQuestionsRepository
    {
        Task<List<QuestionDto>> GetSurveyAsync();
    }
}
