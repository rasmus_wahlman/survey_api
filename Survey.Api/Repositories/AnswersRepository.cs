﻿using Microsoft.EntityFrameworkCore;
using Survey.Api.Data;
using Survey.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.Api.Repositories
{
    public class AnswersRepository : IAnswersRepository
    {
        private readonly DataContext _dataContext;

        public AnswersRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<ResultDto>> GetResultAsync()
        {
            var resultPerQuestion = await _dataContext.Answers
                .GroupBy(a => a.QuestionId)
                .Select(x => new ResultDto 
                { 
                    QuestionId = x.Key, 
                    Count = x.Count(), 
                    Sum = x.Sum(s => s.Grade)
                }).ToListAsync();

            return resultPerQuestion;
        }

        public async Task<List<AnswerDto>> GetResultFromSurveyById(Guid id)
        {
            var resultFromSurvey = await _dataContext.Answers
                .Where(a => a.SurveyId == id)
                .Include(q => q.Question)
                .ToListAsync();

            return resultFromSurvey;
        }

        public async Task<Guid> CreateAnswersAsync(List<AnswerDto> answers)
        {
            var newSurveyId = Guid.NewGuid();
            answers.ForEach(a => a.SurveyId = newSurveyId);

            await _dataContext.Answers.AddRangeAsync(answers);
            var created = await _dataContext.SaveChangesAsync();

            if(created == 0)
            {
                return Guid.Empty;
            } 

            return newSurveyId;
        }
    }
}
