﻿using System.Collections.Generic;

namespace Survey.Api.Contracts.Responses
{
    public class SurveyResultResponse
    {
        public int NumberOfSurveysTaken { get; set; }
        public List<Statistic> AverageGrades { get; set; }
        public List<AnswerResponse> Answered { get; set; }
        public List<QuestionResponse> Questions { get; set; }
    }
}
