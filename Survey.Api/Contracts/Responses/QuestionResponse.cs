﻿using System;

namespace Survey.Api.Contracts.Responses
{
    public class QuestionResponse
    {
        public Guid Id { get; set; }
        public string QuestionKey { get; set; }
        public string Text { get; set; }
    }
}
