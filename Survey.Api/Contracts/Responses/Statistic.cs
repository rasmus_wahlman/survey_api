﻿using System;

namespace Survey.Api.Contracts.Responses
{
    public class Statistic
    {
        public Guid QuestionId { get; set; }
        public decimal AverageGrade { get; set; }
    }
}
