﻿using System;

namespace Survey.Api.Contracts.Responses
{
    public class AnswerResponse
    {
        public Guid QuestionId { get; set; }
        public int Grade { get; set; }
    }
}
