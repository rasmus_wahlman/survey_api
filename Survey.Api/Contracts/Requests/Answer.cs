﻿using System;

namespace Survey.Api.Contracts.Requests
{
    public class Answer
    {
        public Guid QuestionId { get; set; }
        public int Grade { get; set; }
    }
}
