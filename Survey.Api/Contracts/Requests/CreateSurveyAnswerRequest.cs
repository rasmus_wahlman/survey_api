﻿using System.Collections.Generic;

namespace Survey.Api.Contracts.Requests
{
    public class CreateSurveyAnswerRequest
    {
        public List<Answer> Answers { get; set; }
    }
}
