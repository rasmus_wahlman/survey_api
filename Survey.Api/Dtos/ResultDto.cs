﻿using System;

namespace Survey.Api.Dtos
{
    public class ResultDto
    {
        public Guid QuestionId { get; set; }
        public int Sum { get; set; }
        public int Count { get; set; }
    }
}
