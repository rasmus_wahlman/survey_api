﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Survey.Api.Dtos
{
    public class AnswerDto
    {
        [Key]
        public Guid Id { get; set; }

        public int Grade { get; set; }

        public Guid SurveyId { get; set; }

        public Guid QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public QuestionDto Question { get; set; }
    }
}
