﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Survey.Api.Dtos
{
    public class QuestionDto
    {
        [Key]
        public Guid Id { get; set; }
        public string QuestionKey { get; set; }
        public string Text { get; set; }
    }
}
