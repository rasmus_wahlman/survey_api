using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Survey.Api.Data;
using Survey.Api.Dtos;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var serviceScope = host.Services.CreateScope()) 
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                await context.Database.MigrateAsync();

                if (!context.Questions.Any())
                {
                    // Add questions to db
                    await context.Questions.AddAsync(new QuestionDto { QuestionKey = "Q1", Text = "Would you recommend this app to friend?" });
                    await context.Questions.AddAsync(new QuestionDto { QuestionKey = "Q2", Text = "Do you trust your leadership?" });
                    await context.Questions.AddAsync(new QuestionDto { QuestionKey = "Q3", Text = "How much do you like yourself as a colleague?" });
                    await context.Questions.AddAsync(new QuestionDto { QuestionKey = "Q4", Text = "Do you have fun in this app?" });
                    await context.Questions.AddAsync(new QuestionDto { QuestionKey = "Q5", Text = "How much do you want to submit?" });

                    await context.SaveChangesAsync();
                }
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

    }
}
