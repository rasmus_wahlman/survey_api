﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Survey.Api.Data;
using Survey.Api.Dtos;
using System.Linq;


namespace Survey.Api.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<DataContext>));

                services.Remove(descriptor);

                services.AddDbContext<DataContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<DataContext>();
                    var logger = scopedServices
                        .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    db.Database.EnsureCreated();

                    if(!db.Questions.Any())
                    {

                        db.Questions.Add(new QuestionDto { QuestionKey = "Q1", Text = "Would you recommend this app to friend?" });
                        db.Questions.Add(new QuestionDto { QuestionKey = "Q2", Text = "Do you trust your leadership?" });
                        db.Questions.Add(new QuestionDto { QuestionKey = "Q3", Text = "How much do you like yourself as a colleague?" });
                        db.Questions.Add(new QuestionDto { QuestionKey = "Q4", Text = "Do you have fun in this app?" });
                        db.Questions.Add(new QuestionDto { QuestionKey = "Q5", Text = "How much do you want to submit?" });
                    }


                    db.SaveChanges();
                }
            });
        }
    }
}
