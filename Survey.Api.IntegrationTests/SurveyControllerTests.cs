﻿using FluentAssertions;
using Newtonsoft.Json;
using Survey.Api.Contracts.Requests;
using Survey.Api.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace Survey.Api.IntegrationTests
{
    public class SurveyControllerTests : CustomWebApplicationFactory<Startup>
    {
        private readonly HttpClient TestClient;
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public SurveyControllerTests()
        {
            _factory = new CustomWebApplicationFactory<Startup>();
            TestClient = _factory.CreateClient();
        }

        [Fact]
        public async Task GetSurvey_ReturnsSurvey_WhenSurveyExistsInTheDatabase()
        {
            // Act
            var response = await TestClient.GetAsync("/Survey");

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var questions = JsonConvert.DeserializeObject<List<QuestionResponse>>(await response.Content.ReadAsStringAsync());
            questions.Count.Should().Be(5);
        }

        [Fact]
        public async Task CreateSurveyAnswer_ReturnsSurveyStatistic()
        {
            // Arrange
            var request = new CreateSurveyAnswerRequest 
            {
                Answers = new List<Answer> 
                {
                    new Answer { Grade = 1, QuestionId = Guid.NewGuid() },
                    new Answer { Grade = 2, QuestionId = Guid.NewGuid() },
                    new Answer { Grade = 3, QuestionId = Guid.NewGuid() },
                    new Answer { Grade = 4, QuestionId = Guid.NewGuid() },
                    new Answer { Grade = 5, QuestionId = Guid.NewGuid() },
                }
            };

            var dataAsString = JsonConvert.SerializeObject(request);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            // Act
            var response = await TestClient.PostAsync("/Survey", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            var surveyId = JsonConvert.DeserializeObject<Guid>(await response.Content.ReadAsStringAsync());
            surveyId.Should().NotBeEmpty();
        }
    }
}
